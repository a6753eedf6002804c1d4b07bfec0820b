# How to properly delete your Reddit account

After countless protests from the community and greed (not to mention arrogance) from the CEO of Reddit, enough is enough! It's time to leave this website, and show them no mercy.

This guide is for those of you who are finally fed up with the company having no regard for their users, and who are now ready to leave.

## Prerequisites
- A desktop browser
- A little tech-savvyness, or at least being comfortable using the developer tools in your browser
- Patience

## Step 1. Acquire all your Reddit data
In order for proper deletion, you must first get ahold of all your user data on Reddit. With GDPR regulations, [you can do so by following this link.](https://support.reddithelp.com/hc/en-us/articles/360043048352-How-do-I-request-a-copy-of-my-Reddit-data-and-information-)

Unfortunately, it will take some time before the data is ready. For me, it took around 3 weeks. Be patient.

## Step 2a. Deleting all your content
Simply deleting your account will not do. When you delete your account without first deleting your content, your content is not deleted. [Quote from reddit:](https://support.reddithelp.com/hc/en-us/articles/360043047932-If-I-delete-my-account-what-happens-to-my-username-posts-and-comments-)

> - **Your posts and comments become disassociated.**
>
>   Any posts or comments you made from your deleted account stay on Reddit, but people can’t see who they came from. (If you have posts or comments you’d like to remove, you need to delete them before you delete your account.) Please note that when you delete a post, the link to the post will still exist, but the post itself is deleted. The post is only accessible with a direct link and will not appear in Reddit search.

In other words, you need to manually delete all your content. This can however, not be done from your profile page in most cases as you do not actually get to see older content from your profile. This is why you need the data dump mentioned earlier.

## Step 2b. Acquire the ids of your posts and comments.

Now that you have the data dump, you should have two files, one called `posts.csv` and `comments.csv`. Open these files in your favorite spreadsheet program of choice, and copy the first column in both to somewhere you can easily access.

Keep them labeled so you know which set of ids are posts and which are comments.

**Make sure you keep the linebreaks per id.** Your copied text should look something like:
```
id
ba4d7e
78e3mj
7d4ony
9bkmvz
5h1zvq
69g353
5arvaf
6h5p7y
5h5qlv
ax7qb8
7il3hx
6u30ty
59w2d5
6o0vsq
9xvqbf
5rorul
7xxdcl
tbk7n
ofi3wf
```

## Step 2c. Start the content deletion process

Go to the overview (profile page) of your account on the old reddit layout, and open the developer tools in your browser (usually done by pressing F12). Select "Console" and copy paste all the code from `fuck-spez.js` from this gist into the console.

Now, to start the deletion process, you'll type this and press enter:
```js
deleteAll(`your post ids here`, `your comment ids here`);
```

Replace "your post ids here" and "your comment ids here" with your actual ids. **Keep the backtics.**

<details>
<summary>Example</summary>

```js
deleteAll(`id
ba4d7e
78e3mj
7d4ony
9bkmvz
5h1zvq
69g353
5arvaf
6h5p7y
5h5qlv
ax7qb8
7il3hx
6u30ty
59w2d5
6o0vsq
9xvqbf
5rorul
7xxdcl
tbk7n
ofi3wf`, `id
c2tupzg
c4pp159
c2ttjpv
c2wzms5
c9pw07v
c4l64nx
c4l9ue6
ci9btwh
c9pw3s8
c2tzeve
c4pp44s
c2tul9t
c2tsdo2
c4ppgz7
c3k6o1g
c2ts1mi
c4l4x8c
c9pijgq
c2tryr9
cryyblt
crzafyg
c2ts66h
c4pp91m
cijme9j
c4l6ba3
`);
```
</details>

Now just wait, and all your content should be first edited to gibberish (to avoid Reddit simply restoring your content by unflagging it as deleted), then deleted.

Depending on how big your account is, this might take several hours. Keep the tab open and go do something else, and check back periodically to see how much is left.

When it is done, your console should say "All done!" and your profile should look like this after a refresh:

![Purged reddit profile with red text that says "there doesn't seem to be anything here"](https://cdn.discordapp.com/attachments/461795968304545804/1126546299035062364/image.png)

## Step 3. Delete your account

With a purged reddit profile, you are now ready to finally delete your account! We're almost done!

[Follow this link, and follow the instructions to delete your reddit account.](https://support.reddithelp.com/hc/en-us/articles/204579509-How-do-I-delete-my-account-)

## (Optional) Step 4. Show your support
Feel free to post a comment in this gist with a screenshot of your purged reddit profile to show support to the moderators, developers of third party apps, and not to mention the disabled people who are affected by the API changes.

## Conclusion

Congratulations! You are now finally free from the snoo. Where to go from here? Well, if you still want something close to the Reddit experience, the Fediverse is a good option. Check out Lemmy or kbin if you're curious.