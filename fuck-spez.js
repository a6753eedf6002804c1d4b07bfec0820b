// Certain subreddits will not let you edit posts to pure gibberish. This string of text tries to "trick" those filters.
const ANTI_SPAM_FILTER_CONTENT = `In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available. Wikipedia`;

const getRandomString = () =>
  Math.floor(
    Math.random() *
      parseInt(
        "fuckspezfuckspezfuckspezfuckspezfuckspezfuckspezfuckspezfuckspezfuckspez",
        36,
      ),
  ).toString(36);
const zzz = (ms) => new Promise((r) => setTimeout(r, ms));

const getFormattedId = (type, id) => {
  const prefix = {
    post: "t3",
    comment: "t1",
  }[type];

  return `${prefix}_${id}`;
};

async function editContent(options) {
  const { csrfToken, type, id, newContent } = options;

  const response = await fetch("https://www.reddit.com/api/editusertext", {
    method: "post",
    headers: {
      "content-type": "application/x-www-form-urlencoded; charset=utf-8",
    },
    body: new URLSearchParams({
      thing_id: getFormattedId(type, id),
      text: newContent,
      uh: csrfToken,
    }).toString(),
  });

  try {
    let json = await response.json();

    if (!json.success) {
      if (type == "comment") {
        return console.warn(`Failed to edit comment!`);
      }

      console.warn(
        `Failed to edit post! It might not be a selftext post, in which case this warning can be ignored`,
      );
    }
  } catch {}
}

async function deleteContent(options) {
  const { csrfToken, type, id } = options;

  await fetch("https://www.reddit.com/api/del", {
    method: "post",
    headers: {
      "content-type": "application/x-www-form-urlencoded; charset=utf-8",
    },
    body: new URLSearchParams({
      thing_id: getFormattedId(type, id),
      uh: csrfToken,
    }).toString(),
  });
}

async function purgeContentByIds(csrfToken, type, idsString) {
  const ids = idsString
    .trim()
    .split("\n")
    .filter((x) => x != "id");
    
  console.info(`Starting garbling and deletion of ${ids.length} ${type}s`);

  let deleted = 0;
  for (const id of ids) {
    console.log(`Deleting https://redd.it/${id}`);

    await editContent({
      csrfToken,
      id,
      type,
      newContent: ANTI_SPAM_FILTER_CONTENT + getRandomString(),
    });
    await zzz(1000);
    await deleteContent({ csrfToken, id, type });

    deleted += 1;
    console.info(
      `Deleted ${deleted}/${ids.length} ${type}s (${Math.floor(
        (deleted / ids.length) * 100,
      )}%)`,
    );

    await zzz(3000);
  }

  console.info(`Finished deleting ${ids.length} ${type}s!`);
}

async function deleteAll(posts, comments) {
  const csrfToken = document.querySelector("input[name=uh]").value;

  if (!csrfToken) {
    throw new Error("CSRF token missing! Are you on the old layout?");
  }

  await purgeContentByIds(csrfToken, "post", posts);
  await zzz(1000);
  await purgeContentByIds(csrfToken, "comment", comments);

  console.info("All done!");
}